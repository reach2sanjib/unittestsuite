package TestClasses;

import static org.junit.Assert.*;

import org.junit.Test;

import sourcecode.ModuleOne;


public class CalcTest {
	@Test
	public void addTest() {
		ModuleOne myCalc = new ModuleOne();
		int result = myCalc.addNumbers(5, 5);
		assertEquals(10,result);
	}

	@Test
	public void subtractTest() {
		ModuleOne myCalc = new ModuleOne();
		assertEquals(5, myCalc.subtractNumbers(10, 5));
	}
	
	@Test
	public void whenPalindrom_thenAccept() {
		ModuleOne palindromeTester = new ModuleOne();
	    assertTrue(palindromeTester.isPalindrome("noon"));
	}
	     
	@Test
	public void whenNearPalindrom_thanReject(){
		ModuleOne palindromeTester = new ModuleOne();
	    assertFalse(palindromeTester.isPalindrome("neon"));
	}
}
